var bonjour = require("bonjour")();
var test = require("tape");

var oitDevice = require("../");

var testId = "" + Math.round(Math.random() * 10000);
var toggleState = 0;

var oitService = oitDevice([{
  type: 1,

  getValue: function() {
    return toggleState;
  },

  setValue: function(v, cb) {
    toggleState = v ? 1 : 0;

    cb();
  }
}], {
  adOpts: {
    txtRecord: {
      oitTest: testId
    }
  }
});

test(function(t) {
  t.equal(typeof oitService.start, "function", "oitDevice() returns function");

  t.end();
}, "basic");

test(function(t) {
  t.plan(1);

  var browser = bonjour.find({
    type: "oitdevice",
    protocol: "tcp"
  });

  browser.on("up", onServiceUp);
  browser.start();

  oitService.start();

  function onServiceUp(service) {
    // dont' get confused by non-test devices
    if(!service.txt.oittest) return;

    t.equal(testId, service.txt.oittest, "found service");
    browser.stop();

    browser.removeListener("up", onServiceUp);
  }
}, "setValCB");

test(function(t) {
  t.plan(1);

  oitService.stop(function() {
    t.pass("Oit Service stopped");
    bonjour.destroy();
  });
}, "cleanup");
