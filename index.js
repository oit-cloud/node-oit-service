var async = require("async");
var bonjour = require("bonjour")();
var debug = require("debug")("oit-service");
var net = require("net");

module.exports = function(services, opts) {
  opts = opts || {};

  var port = opts.port || 8448;

  var oitAdvert = bonjour.publish({
    name: "oitdevice",
    port: port,
    type: "oitdevice",
    txt: (opts.adOpts || {}).txtRecord
  });

  var server = net.createServer().listen({
    port: port,
    host: opts.host
  });

  debug("Listening on %s:%s", opts.host, port);

  server.on("error", function(e) {
    console.trace(e);
  });

  server.on("connection", function(sock) {
    sock.on("data", function(chunk) {
      var seq = chunk[0];
      var cmd = chunk[1];
      var index = chunk[2];
      var value = chunk[3];

      debug("seq: %s", seq);
      debug("cmd: %s", cmd);
      debug("value: %s", value);

      var reply = new Buffer(32).fill(0);

      reply[0] = seq;
      reply[1] = cmd;

      switch(cmd) {
        case 0:
          services.forEach(function(service, idx) {
            reply[(idx * 3) + 2] = idx;
            reply[(idx * 3) + 3] = service.type || 0xff;
            reply[(idx * 3) + 4] = service.getValue();
          });

          sock.write(reply);
        break;

        case 1:
          services[index].setValue(value, function(err) {
            reply[3] = err ? 0 : 1;
            reply[4] = value;

            services[index].value = value;

            sock.write(reply);
          });
        break;
      }
    });
  });

  return {
    start: function() {
      oitAdvert.start();
    },

    stop: function(cb) {
      async.series([
        function(done) {
          debug("unpublishing");
          bonjour.unpublishAll();

          debug("stopping advertisement");
          oitAdvert.stop();
          done();
        },

        function(done) {
          debug("closing server");
          server.close(done);
        },

        function(done) {
          debug("destroying bonjour");
          bonjour.destroy();
          done();
        }
      ], cb);
    }
  }
}
